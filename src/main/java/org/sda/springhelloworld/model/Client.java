package org.sda.springhelloworld.model;

public class Client {
	private String adress;

	@Override
	public String toString() {
		return "Client [adress=" + adress + "]";
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Client(String adress) {

		this.adress = adress;
	}

	public Client() {

	}
	public void afterInit() {
		System.out.println("Client has been initialized.");
	}
	
	public void beforeDestroy() {
		System.out.println("This is the end for the client.");
	}


}
