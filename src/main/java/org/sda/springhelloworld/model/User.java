package org.sda.springhelloworld.model;

public class User {
	private String firstName;
	private String lastName;
	private Adress adress;

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void printMessage() {
		System.out.println(firstName + " " + lastName + " " + adress);

	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + "]";
	}

	public User(String firstName, String lastName, Adress adress) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
	}

	public User() {

	}
}
