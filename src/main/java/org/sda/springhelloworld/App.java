package org.sda.springhelloworld;

import java.util.Map.Entry;

import org.sda.springhelloworld.model.Client;
import org.sda.springhelloworld.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class App {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
		MessagePrinter messagePrinter = (MessagePrinter) applicationContext.getBean("messagePrinter");
		System.out.println(messagePrinter.getMessage());
		messagePrinter.printMessage();

		MessagePrinter messagePrinter2 = (MessagePrinter) applicationContext.getBean("messagePrinter");
		System.out.println(messagePrinter.getMessage());
		messagePrinter.printMessage();

		// Prototype bean with id=user1
		User user1 = (User) applicationContext.getBean("user1");
		User different = (User) applicationContext.getBean("user1");
		System.out.println(user1);
		System.out.println(different);

		// singeleton bean with id=user2
		User user2 = (User) applicationContext.getBean("user2");
		User sameUser2 = (User) applicationContext.getBean("user2");
		System.out.println(user2);
		System.out.println(sameUser2);

		System.out.println(user1.getFirstName() + " " + user1.getLastName());
		System.out.println(user2);

		System.out.println(messagePrinter);
		System.out.println(messagePrinter2);

		UserService userService = (UserService) applicationContext.getBean("userService");
		// System.out.println(userService.getClient());
		System.out.println(userService.getUser().getFirstName() + " " + userService.getUser().getLastName());
		System.out.println(userService.getClient().getAdress());

		for (Client client : userService.getClients()) {
			System.out.println(client.getAdress());

		}

				for (Entry<User, Client> entry : userService.getPeople().entrySet())
				{
				    System.out.println(entry.getKey() + " " + entry.getValue());
				}
				((AbstractApplicationContext) applicationContext).registerShutdownHook();
				
				User user3=(User) applicationContext.getBean("user3");
				System.out.println(user3);

	}
	
}
