package org.sda.springhelloworld;

import java.util.Map;
import java.util.List;

import org.sda.springhelloworld.model.Client;
import org.sda.springhelloworld.model.User;

public class UserService {

	private Map<User, Client> people;

	private Client client;

	private User user;
	private List<Client> clients;

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public User getUser() {
		return user;
	}

	public Map<User, Client> getPeople() {
		return people;
	}

	public void setPeople(Map<User, Client> people) {
		this.people = people;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserService(User user) {
		this.user = user;
	}

	public UserService(Client client) {
		this.client = client;
	}

	public UserService() {

	}

}
